#Basic Items Mod for Terraria

This mod is a bit of a 'pet project' of mine. It aims to add various 'basic' things to Terraria (items, mechanics, classes, etc.) that I feel are missing from the game. This mod is in alpha at this time but is being developed.

It uses [TModLoader](https://github.com/blushiemagic/tModLoader) as a middleware manager and API between the native game and this mod.

I am attempting to balance this against vanilla. However, doing so is not my top priority; which is making the classes, mechanics, items, etc. fun to play with.

If you have any input you would like to add, you can reach me in the [TModLoader Discord](https://discord.gg/RMZCqq6) under the user **PerfidiousLeaf#7408**.