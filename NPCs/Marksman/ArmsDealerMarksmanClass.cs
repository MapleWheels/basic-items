﻿using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace BasicItems.NPCs.Marksman
{
    public class ArmsDealerMarksmanClass : GlobalNPC
    {
        public override bool InstancePerEntity => true;

        public override void SetupShop(int type, Chest shop, ref int nextSlot)
        {
            if (type == NPCID.ArmsDealer)
            {
                //---Winchester-73 - Gun
                shop.item[nextSlot].SetDefaults(
                    mod.ItemType<Items.Weapons.Guns.MarksmanRifle00_Winchester73>());
                shop.item[nextSlot].shopCustomPrice = new int?(50000);
                nextSlot++;
                //---Winchester-73 - .38-40 Ammo
                shop.item[nextSlot].SetDefaults(
                    mod.ItemType<Items.Weapons.Projectiles.MarksmanAmmo00_3840>());
                shop.item[nextSlot].shopCustomPrice = new int?(5);
                nextSlot++;

                if (NPC.downedBoss1)    //Eye of Cthulu
                {
                    //---Winchester-73B - .357 Ammo
                    shop.item[nextSlot].SetDefaults(
                        mod.ItemType<Items.Weapons.Projectiles.MarksmanAmmo00B_357>());
                    shop.item[nextSlot].shopCustomPrice = new int?(7);
                    nextSlot++;
                }

                if (NPC.downedBoss2)    //Brain or Eater
                {
                    //---STG-44 - Gun
                    shop.item[nextSlot].SetDefaults(
                        mod.ItemType<Items.Weapons.Guns.MarksmanRifle01_Sturmgewehr44>());
                    shop.item[nextSlot].shopCustomPrice = new int?(170000);
                    nextSlot++;
                    //---STG-44 - 8mm Ammo
                    shop.item[nextSlot].SetDefaults(
                        mod.ItemType<Items.Weapons.Projectiles.MarksmanAmmo01_8mm>());
                    shop.item[nextSlot].shopCustomPrice = new int?(9);
                    nextSlot++;
                }

                if (Main.hardMode)    //Hardmode, Wall of Flesh
                {
                    //---AR-15 - Gun
                    shop.item[nextSlot].SetDefaults(
                        mod.ItemType<Items.Weapons.Guns.MarksmanRifle02_Armalite15>());
                    shop.item[nextSlot].shopCustomPrice = new int?(330000);
                    nextSlot++;
                    //---AR-15 - .223 Ammo
                    shop.item[nextSlot].SetDefaults(
                        mod.ItemType<Items.Weapons.Projectiles.MarksmanAmmo02_223>());
                    shop.item[nextSlot].shopCustomPrice = new int?(13);
                    nextSlot++;
                }

                if (NPC.downedPlantBoss)    //Plantera
                {
                    //---AR-10_M5E1 - Gun
                    shop.item[nextSlot].SetDefaults(
                        mod.ItemType<Items.Weapons.Guns.MarksmanRifle03_M5E1>());
                    shop.item[nextSlot].shopCustomPrice = new int?(850000);
                    nextSlot++;
                    //---AR-10_M5E1 - .308 Ammo
                    shop.item[nextSlot].SetDefaults(
                        mod.ItemType<Items.Weapons.Projectiles.MarksmanAmmo03_308>());
                    shop.item[nextSlot].shopCustomPrice = new int?(85);
                    nextSlot++;
                }
            }
        }
    }
}
