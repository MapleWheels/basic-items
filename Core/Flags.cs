﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicItems.Core
{
    //---Class Flag Config Structs---//
    //Contains the data loaded from the Configuration File. If the data is unavailable
    //or omitted, the default is used. Used with the Config class.
    public class _Flags
    {
        public class _Items
        {
            /// <summary>
            /// These items are legacy code, with bugs, from when I first started modding.
            /// They normally unattainable unless Production Mode flag has been reset
            /// (Config._bProductionMode = false)
            /// </summary>
            public class _BasicWeapons
            {
                public class _BasicBullet
                {
                    public bool Enabled;
                    public int Damage;
                    public int CriticalChance;
                    public float Knockback;
                    public int MaxStackSize;

                    public _BasicBullet(
                        bool Enabled = true, 
                        int Damage = 7, 
                        int CriticalChance = 15,
                        float Knockback = 2f, 
                        int MaxStackSize = 500)
                    {
                        //Defaults
                        this.Enabled = Enabled;
                        this.Damage = Damage;
                        this.CriticalChance = CriticalChance;
                        this.Knockback = Knockback;
                        this.MaxStackSize = MaxStackSize;
                    }
                }
                //BasicWeapons:Instances
                public _BasicBullet BasicBullet;

                public _BasicWeapons(_BasicBullet BasicBullet = null)
                {
                    this.BasicBullet = BasicBullet ?? new _BasicBullet();
                }
            }

            /// <summary>
            /// Contains the defaults data on various Equippable Items and Accessories.
            /// </summary>
            public class _Equip
            {
                /// <summary>
                /// Contains Marksman class equips.
                /// </summary>
                public class _Marksman
                {
                    /// <summary>
                    /// Contains the defaults for the Moarksman Sigils. Sigils apply on hit effects based
                    /// on a player-side stack counter. Details can be found in the  
                    /// BasicItems.Equip.Marksman.Sigils namespace classes.
                    /// </summary>
                    public class _Sigils
                    {
                        /// <summary>
                        /// Holds the data for the Sigil Instances.
                        /// </summary>
                        public class _RankDef
                        {
                            public int DamageBonusFlatPerStack;
                            public float DamageBonusFactorPerStack;
                            public int CritBonusFlatPerStack;
                            public float CritBonusFactorPerStack;
                            public int DefenseReductionFlatPerStack;
                            public float DefenseReductionFactorPerStack;
                            public int MaxStacks;

                            public bool Ichor;
                            public bool Fire;
                            public bool FrostBurn;

                            /// <summary>
                            /// Constructor for Sigil Definitions. Multipliers are applied as taken to the
                            /// power of the amount of stacks the player has.
                            /// </summary>
                            /// <param name="DamageBonusFlatPerStack">Flat Damage increase.</param>
                            /// <param name="DamageBonusFactorPerStack">Damage multiplier.</param>
                            /// <param name="CritBonusFlatPerStack">Flat Critical Chance increase.</param>
                            /// <param name="CritBonusFactorPerStack">Critical Chance multiplier.</param>
                            /// <param name="DefenseReductionFlatPerStack">Flat target Defense reduction.</param>
                            /// <param name="DefenseReductionFactorPerStack">Target Defense multiplier.</param>
                            /// <param name="MaxStacks">The maximum amount of stacks.</param>
                            /// <param name="Ichor">Whether or not the Ichor Debuff is applied at maximum stacks.</param>
                            /// <param name="Fire"Whether or not the OnFire! Debuff is applied at maximum stacks.></param>
                            /// <param name="FrostBurn">Whether or not the Frost Burn Debuff is applied at maximum stacks.</param>
                            public _RankDef(int DamageBonusFlatPerStack,
                                float DamageBonusFactorPerStack,
                                int CritBonusFlatPerStack,
                                float CritBonusFactorPerStack,
                                int DefenseReductionFlatPerStack,
                                float DefenseReductionFactorPerStack,
                                int MaxStacks, bool Ichor, bool Fire, bool FrostBurn)
                            {
                                this.DamageBonusFlatPerStack = (int)(DamageBonusFlatPerStack * _iCalFactor);
                                this.DamageBonusFactorPerStack = (float)Math.Pow(DamageBonusFactorPerStack,_iCalFactor);
                                this.CritBonusFlatPerStack = (int)(CritBonusFlatPerStack * _iCalFactor);
                                this.CritBonusFactorPerStack = (float)Math.Pow(CritBonusFactorPerStack, _iCalFactor);
                                this.DefenseReductionFlatPerStack = (int)(DefenseReductionFlatPerStack * _iCalFactor);
                                this.DefenseReductionFactorPerStack = (float)Math.Pow(DefenseReductionFactorPerStack, _iCalFactor);
                                this.MaxStacks = MaxStacks;
                                this.Ichor = Ichor;
                                this.Fire = Fire;
                                this.FrostBurn = FrostBurn;
                            }
                        }

                        public _RankDef Rank00;
                        public _RankDef Rank01;
                        public _RankDef Rank02;
                        public _RankDef Rank03;
                        public _RankDef Rank04;
                        public _RankDef Rank05;
                        public _RankDef Rank06;
                        public _RankDef Rank07;
                        public _RankDef Rank08;
                        public _RankDef Rank09;
                        public _RankDef Rank10;
                        public _RankDef Rank11;
                        public _RankDef Rank12;

                        public int StackDecayTimeTicks;

                        /// <summary>
                        /// The default values for all of the Sigils.
                        /// </summary>
                        /// <param name="Rank00">The starting Sigil, available after Iron is found.</param>
                        /// <param name="Rank01">Available after the Eye of Cthulu is defeated.</param>
                        /// <param name="Rank02">Available after the Brain of Cthulu/Eater of Worlds is defeated.</param>
                        /// <param name="Rank03">Available after Skeletron is defeated.</param>
                        /// <param name="Rank04">Available after the Wall of Flesh is defeated.</param>
                        /// <param name="Rank05">Available after 1 Mech Boss is defeated.</param>
                        /// <param name="Rank06">Available after 2 Mech Bosses is defeated.</param>
                        /// <param name="Rank07">Available after 3 Mech Bosses is defeated.</param>
                        /// <param name="Rank08">Available after Plantera is defeated.</param>
                        /// <param name="Rank09">Available after Duke Fisheron is defeated.</param>
                        /// <param name="Rank10">Available after Golem is defeated.</param>
                        /// <param name="Rank11">Available after the Lunar Cultists are defeated.</param>
                        /// <param name="Rank12">Available after the Moon Lord is defeated.</param>
                        /// <param name="StackDecayTimeTicks">The time in ticks for linear decay of the player's stacks.</param>
                        public _Sigils(_RankDef Rank00 = null, _RankDef Rank01 = null,
                            _RankDef Rank02 = null, _RankDef Rank03 = null, _RankDef Rank04 = null,
                            _RankDef Rank05 = null, _RankDef Rank06 = null, _RankDef Rank07 = null,
                            _RankDef Rank08 = null, _RankDef Rank09 = null, _RankDef Rank10 = null,
                            _RankDef Rank11 = null, _RankDef Rank12 = null, int StackDecayTimeTicks = (int)(3.5f*60f))
                        {
                            //All Per Stack: DamageFlat, DamageMulti, CritFlat, CritMulti, TargetDefLoss, TargetDefMulti, MaxStacks, Ichor?, Fire?, Poison?
                            this.Rank00 = Rank00 ?? new _RankDef(1, 1.000f, 1, 1.000f, 1, 1.000f, 2, false, false, false);  //game start
                            this.Rank01 = Rank01 ?? new _RankDef(2, 1.000f, 1, 1.000f, 1, 1.000f, 2, false, false, false);  //Eye of cthulu
                            this.Rank02 = Rank02 ?? new _RankDef(2, 1.000f, 2, 1.000f, 1, 1.000f, 3, false, false, false);  //Brain/Eater
                            this.Rank03 = Rank03 ?? new _RankDef(2, 1.000f, 2, 1.001f, 1, 1.000f, 4, false, false, false);  //Skeletron
                            this.Rank04 = Rank04 ?? new _RankDef(3, 1.003f, 3, 1.003f, 1, 1.000f, 3, false, false, false);  //Wall of flesh
                            this.Rank05 = Rank05 ?? new _RankDef(3, 1.006f, 3, 1.004f, 1, 1.000f, 4, false, false, false);  //1 Mech Boss
                            this.Rank06 = Rank06 ?? new _RankDef(4, 1.008f, 3, 1.005f, 1, 1.000f, 4, false, false, false);  //2 Mech Bosses
                            this.Rank07 = Rank07 ?? new _RankDef(3, 1.010f, 3, 1.007f, 1, 0.997f, 5, false, false, false);  //3 Mech Bosses
                            this.Rank08 = Rank08 ?? new _RankDef(4, 1.012f, 3, 1.009f, 2, 0.996f, 5, false, true, false);  //Plantera
                            this.Rank09 = Rank09 ?? new _RankDef(4, 1.015f, 3, 1.011f, 2, 0.994f, 5, false, true, false);  //Duke fisheron
                            this.Rank10 = Rank10 ?? new _RankDef(4, 1.018f, 3, 1.012f, 2, 0.993f, 6, true, true, false);  //Golem
                            this.Rank11 = Rank11 ?? new _RankDef(5, 1.021f, 3, 1.015f, 2, 0.991f, 6, true, true, true);  //Lunar Cultist
                            this.Rank12 = Rank12 ?? new _RankDef(6, 1.030f, 4, 1.022f, 3, 0.989f, 7, true, true, true);  //Moon lord

                            this.StackDecayTimeTicks = StackDecayTimeTicks;
                        }
                    }

                    public _Sigils Sigils;

                    public _Marksman(_Sigils Sigils = null)
                    {
                        this.Sigils = Sigils ?? new _Sigils();
                    }
                }

                /// <summary>
                /// Dead Man's plate is an item that increase movement speed, defense and damage
                /// based on charges that are gained while moving. When the player attacks, 
                /// the charges are lost.
                /// </summary>
                public class _DeadMansPlate
                {
                    public bool Enabled;
                    public float ChargeRatePerTick;
                    public float ChargeToSpeedFactor;
                    public float ChargeToDefenseFactor;
                    public float ChargeToDamageFactor;
                    public int MaxCharges;
                    public int PassiveDefense;

                    public _DeadMansPlate(
                        bool Enabled = true, 
                        float ChargeRatePerTick = 0,//defined in contructor
                        float ChargeToSpeedFactor = .05f, 
                        float ChargeToDefenseFactor = .07f,
                        float ChargeToDamageFactor = .4f, 
                        int MaxCharges = 100,
                        int PassiveDefense = 10)
                    {
                        this.Enabled = Enabled;
                        this.MaxCharges = MaxCharges;
                        this.ChargeRatePerTick = (float)this.MaxCharges / (10.0f* 60.0f /*secs * tps*/);
                        this.ChargeToSpeedFactor = ChargeToSpeedFactor;
                        this.ChargeToDefenseFactor = ChargeToDefenseFactor * _iCalFactor;
                        this.ChargeToDamageFactor = ChargeToDamageFactor * _iCalFactor;
                        this.PassiveDefense = (int)(PassiveDefense * _iCalFactor);
                    }
                }

                /// <summary>
                /// Mana Fairy redirects a percent of incoming damage to the player's mana bar
                /// based on how much health the player has.
                /// </summary>
                public class _ManaFairy
                {
                    public bool Enabled;
                    public float DamageToManaRatioMin;
                    public float DamageToManaRatioMax;
                    public float DamageToManaMinHP;
                    public int PlayerManaBonus;
                    public float MagicDamageBonusRatio;

                    public _ManaFairy(bool Enabled = true, 
                        float DamageToManaRatioMin = .15f, 
                        float DamageToManaRatioMax = .4f,
                        float DamageToManaMinHP = .33f, 
                        int PlayerManaBonus = 100, 
                        float MagicDamageBonusRatio = .1f)
                    {
                        this.Enabled = Enabled;
                        this.DamageToManaRatioMin = DamageToManaRatioMin;
                        this.DamageToManaRatioMax = DamageToManaRatioMax;
                        this.DamageToManaMinHP = DamageToManaMinHP;
                        this.PlayerManaBonus = PlayerManaBonus;
                        this.MagicDamageBonusRatio = MagicDamageBonusRatio;
                    }
                }

                public class _SterakGauge
                {
                    public bool Enabled;
                    public float ShieldHpPercent;
                    public int ShieldDecayTimeTicks;
                    public float ShiftHpTriggerThreshold;
                    public int ShieldCooldownTimeTicks;
                    public float ShieldHeathConversionFactor;
                    public float ShieldHpPercentDmgTriggerFactor;

                    public _SterakGauge(bool Enabled = true, 
                        float ShieldHpPercent = .4f, 
                        int ShieldDecayTimeTicks = 10 * 60 /*seconds * tps*/,
                        float ShiftHpTriggerThreshold = .5f, 
                        int ShieldCooldownTimeTicks = 120 * 60 /*seconds * tps*/,
                        float ShieldHeathConversionFactor = .5f, 
                        float ShieldHpPercentDmgTriggerFactor = .2f)
                    {
                        this.Enabled = Enabled;
                        this.ShieldHpPercent = ShieldHpPercent;
                        this.ShieldDecayTimeTicks = ShieldDecayTimeTicks;
                        this.ShiftHpTriggerThreshold = ShiftHpTriggerThreshold;
                        this.ShieldCooldownTimeTicks = ShieldCooldownTimeTicks;
                        this.ShieldHeathConversionFactor = ShieldHeathConversionFactor;
                        this.ShieldHpPercentDmgTriggerFactor = ShieldHpPercentDmgTriggerFactor;
                    }
                }
                //Equip:Instances
                public _Marksman Marksman;
                public _DeadMansPlate DeadMansPlate;
                public _ManaFairy ManaFairy;
                public _SterakGauge SterakGauge;

                public _Equip(_Marksman Marksman = null, _DeadMansPlate DeadMansPlate = null, 
                    _ManaFairy ManaFairy = null, _SterakGauge SterakGauge = null)
                {
                    this.Marksman = Marksman ?? new _Marksman();
                    this.DeadMansPlate = DeadMansPlate ?? new _DeadMansPlate();
                    this.ManaFairy = ManaFairy ?? new _ManaFairy();
                    this.SterakGauge = SterakGauge ?? new _SterakGauge();
                }
            }

            public class _Weapons
            {
                public _Weapons() { }
            }
            //Items:Instances
            public _BasicWeapons BasicWeapons;
            public _Equip Equip;
            public _Weapons Weapons;

            public _Items(_BasicWeapons BasicWeapons = null, _Equip Equip = null, _Weapons Weapons = null)
            {
                this.BasicWeapons = BasicWeapons ?? new _BasicWeapons();
                this.Equip = Equip ?? new _Equip();
                this.Weapons = Weapons ?? new _Weapons();
            }
        }
        //Flags:Instances
        public _Items Items;
        public static bool _bCalamityBalance = true;
        public static float _iCalFactor = _bCalamityBalance ? 1.6f : 1f;

        public _Flags(_Items Items = null, bool CalamityLoaded = false)
        {
            this.Items = Items ?? new _Items();
            _bCalamityBalance = CalamityLoaded;
        }
    }
}
