﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BasicItems.Core.Util
{
    public class EnumStringValue : Attribute
    {
        #region Properties
        public string StringValue { get; protected set; }
        #endregion

        #region Constructor
        public EnumStringValue(string value)
        {
            this.StringValue = value;
        }
        #endregion
    }

    public static class EnumAttrib
    {
        public static string GetStringValue(this Enum value)
        {
            Type type = value.GetType();
            FieldInfo fInfo = type.GetField(value.ToString());

            EnumStringValue[] attribs = fInfo.GetCustomAttributes(typeof(EnumStringValue), false) as EnumStringValue[];

            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }
    }
}
