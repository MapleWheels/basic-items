﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BasicItems.Core.Util
{
    public class ModRecipeHelper : ModRecipe
    {
        public Func<bool> OverrideRecipeAvailable;

        public ModRecipeHelper(Mod mod): base(mod)
        {

        }

        public override bool RecipeAvailable()
        {
            return (OverrideRecipeAvailable == null) ? true : OverrideRecipeAvailable();
        }
    }
}
