﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.IO;
using Terraria.ModLoader;
using Terraria.ID;

namespace BasicItems.Core
{

    /// <summary>
    /// This class handles the loading, saving and client-server synchroniztion of
    /// configuration data. The data is stored using the _Flags class and defaults are set there.
    /// </summary>
    public class Config
    {
        //---Control Flags & Variables---//
        public bool _bProductionMode;
        public _Flags Flags { get; private set; }

        /// <summary>
        /// This constructor initializes the members to default values to allow their use in
        /// other sections of code during the mod's bootloading process.
        /// </summary>
        public Config()
        {
            //Defaults, assume single player.
            _bProductionMode = true;
            Flags = new _Flags();
        }

        /// <summary>
        /// Once class initialization is complete, the Bootloader will call this function and
        /// the actual data will begin loading.
        /// </summary>
        /// <param name="mod">The instance of this mod</param>
        /// <returns></returns>
        public bool Load(Mod mod)
        {
            Flags = new _Flags(
                CalamityLoaded: (Bootloader.GetModInstance(Bootloader.ModID.CalamityMod)) == null ? false : true
            );
            return false;
        }

        private bool LoadConfig(out _Flags f)
        {
            f = new _Flags();
            return false;
        }

        private bool SaveConfig(ref _Flags f)
        {
            return false;
        }

        private bool CreateConfig(ref _Flags f)
        {
            return false;
        }
    }
}
