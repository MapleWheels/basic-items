﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;
using Terraria.Localization;
using BasicItems.Core.Util;
using Terraria.UI;
using Terraria.GameContent;
using Terraria.GameContent.UI;
using Terraria.GameContent.UI.Elements;
using Terraria.GameContent.UI.States;
using BasicItems.UI.Weapons;

namespace BasicItems.Core
{
    /// <summary>
    /// This class acts as the Bootstrapper class for the entire mod. This class is loaded by the
    /// ModLoader on startup.
    /// </summary>
    public class Bootloader : Mod
    {
        internal UserInterface WeaponUIInterface;
        internal static Bootloader Instance;
        internal WeaponInfoHUD WeaponUI;

        public Config Cfg { get; private set; }
        /// <summary>
        /// A list of supported mod integrations with enum to string lookup for
        /// update compatibility.
        /// </summary>
        public enum ModID
        {
            [EnumStringValue("CalamityMod")]
            CalamityMod,
            [EnumStringValue("TerrariaOverhaul")]
            TOverhaul
        };

        /// <summary>
        /// Default constructor. Most items are initialized in Load(). However,
        /// anything that is required to be initialized immediately at runtime
        /// is placed here.
        /// </summary>
        public Bootloader()
        {
            Instance = this;
            Properties = new ModProperties()
            {
                Autoload = true,
                AutoloadSounds = true
            };

            Cfg = new Config(); //Primary initialization
        }

        /// <summary>
        /// Mod loading and initialization.
        /// </summary>
        public override void Load()
        {
            if (!Main.dedServ)
            {
                //Weapon UI
                WeaponUI = new WeaponInfoHUD();
                WeaponUI.Activate();
                WeaponInfoHUD.Visible = true; //TODO: Replace with dynamic control via ModPlayer.
                WeaponUIInterface = new UserInterface();
                WeaponUIInterface.SetState(WeaponUI);
            }
        }

        public override void UpdateUI(GameTime gameTime)
        {
            if (WeaponInfoHUD.Visible)
            {
                WeaponUIInterface?.Update(gameTime);
            }
        }

        public override void ModifyInterfaceLayers(List<GameInterfaceLayer> layers)
        {
            //Adding the WeaponHUD UI
            layers.Insert(layers.Count, new LegacyGameInterfaceLayer(
                "BasicItems: WeaponInfoHUD",
                delegate
                {
                    //TODO: Implement UI loading.
                    if (WeaponInfoHUD.Visible)
                    {
                        WeaponUIInterface.Update(Main._drawInterfaceGameTime);
                        WeaponUI.Draw(Main.spriteBatch);
                    }
                    return true;
                }
                ));
        }

        /// <summary>
        /// Loading is done in PostSetupContent() due to mod integration checks. 
        /// </summary>
        public override void PostSetupContent()
        {
            if (!Cfg.Load(this))
            {
                Console.WriteLine("BasicItems: Unable to Load Config, using default values.");
            }
        }

        /// <summary>
        /// Helper method to get the current instance of a compatible other mod if available.
        /// </summary>
        /// <param name="ModID">The Mod's name, derived from the enum.</param>
        /// <returns></returns>
        public static Mod GetModInstance(ModID ModID)
        {
            return ModLoader.GetMod(ModID.GetStringValue());
        }

        public override void AddRecipeGroups()
        {
            //Corruption/Crimson
            RecipeGroup grp = new RecipeGroup(() => Language.GetTextValue("Misc.37") + " Demonite Ore", new int[] {
                ItemID.DemoniteOre,
                ItemID.CrimtaneOre
                
            });
            RecipeGroup.RegisterGroup("BasicItems:DemonOre", grp);

            grp = new RecipeGroup(() => Language.GetTextValue("Misc.37") + " Demonite Bar", new int[] {
                ItemID.DemoniteBar,
                ItemID.CrimtaneBar
            });
            RecipeGroup.RegisterGroup("BasicItems:DemonBar", grp);

            grp = new RecipeGroup(() => Language.GetTextValue("Misc.37") + " Shadow Scale", new int[] {
                ItemID.ShadowScale,
                ItemID.TissueSample
            });
            RecipeGroup.RegisterGroup("BasicItems:ShadowScale", grp);
        }

        public override void AddRecipes()
        {
            //Exchange: Band of Starpower <-> Panic Necklace
            RecipeEditor editor = new RecipeEditor(new Recipe());   
            editor.AddIngredient(ItemID.PanicNecklace);
            editor.SetResult(ItemID.BandofStarpower);
            editor.AddTile(TileID.TinkerersWorkbench);

            editor = new RecipeEditor(new Recipe());
            editor.AddIngredient(ItemID.BandofStarpower);
            editor.SetResult(ItemID.PanicNecklace);
            editor.AddTile(TileID.TinkerersWorkbench);

        }
    }
}
