﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Items;

namespace BasicItems.Items.Weapons.Guns
{
    /// <summary>
    /// Based on the STG-44, the gun that defined the term "Assault Rifle".
    /// </summary>
    /// <remarks>
    /// Uses a custom Item.UseTime tracking system due to a bug.
    /// </remarks>
    public class MarksmanRifle01_Sturmgewehr44 : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("The 'Assault Rifle'-44");
            Tooltip.SetDefault("Could have turned the War.");
        }

        public override void SetDefaults()
        {
            item.damage = 13;
            item.crit = 7;
            item.ranged = true;
            item.width = 46;
            item.height = 17;
            item.useTime = 9; 
            item.useAnimation = 7;
            item.useStyle = 5;
            item.noMelee = true;
            item.knockBack = 4f;
            item.value = 170000;
            item.rare = 2;
            //item.UseSound = SoundID.Item11;
            item.UseSound = mod.GetLegacySoundSlot(SoundType.Item, "Sounds/Item/Weapons/Guns/stg44");
            item.autoReuse = true;
            item.shoot = mod.ProjectileType(nameof(Projectiles.MarksmanAmmo01_8mm_Projectile));
            item.shootSpeed = 34f;
            item.useAmmo = mod.ItemType(nameof(Projectiles.MarksmanAmmo01_8mm));
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                //Disabled, sold at the ArmsDealer
                ////---Production Recipe---//
                //ModRecipe Production = new ModRecipe(mod);
                ////Production.AddIngredient(ItemID.Minishark, 1);
                //Production.AddRecipeGroup("IronBar", 3);
                ////Production.AddIngredient(ItemID.IllegalGunParts, 1);
                ////Production.AddIngredient(mod.ItemType("BasicGun"), 1); Disabled; dev mode only.
                //Production.AddTile(TileID.Anvils);
                //Production.SetResult(this);
                //Production.AddRecipe();
            }
            else
            {
                //---Build Testing Recipe---//
                ModRecipe DevTesting = new ModRecipe(mod);
                DevTesting.SetResult(this);
                DevTesting.AddRecipe();
            }
        }

        public override Vector2? HoldoutOffset()
        {
            return new Vector2(-15, 0);
        }

        public override bool ConsumeAmmo(Player player)
        {
            return true;
            //return Main.rand.NextFloat() > 0f;
        }

        public override bool Shoot(Player player, ref Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)
        {
            Vector2 muzzleOffset = Vector2.Normalize(new Vector2(speedX, speedY) + new Vector2(0, -6)) * 26f;
            position += muzzleOffset;

            Vector2 perturbedSpeed = new Vector2(speedX, speedY).RotatedByRandom(MathHelper.ToRadians(2));
            speedX = perturbedSpeed.X;
            speedY = perturbedSpeed.Y;

            MarksmanRifle01_STG44_ReloadTracker AmmoTracker = player.GetModPlayer<MarksmanRifle01_STG44_ReloadTracker>(mod);
            if (!AmmoTracker.CanFire)
            {
                return false;
            }
            AmmoTracker.CurrentAmmoCount--;
            AmmoTracker.ResetReloadTimer();
            return base.Shoot(player, ref position, ref speedX, ref speedY, ref type, ref damage, ref knockBack);
        }

        public override bool CanUseItem(Player player)
        {
            MarksmanRifle01_STG44_ReloadTracker AmmoTracker = player.GetModPlayer<MarksmanRifle01_STG44_ReloadTracker>(mod);
            return AmmoTracker.CanFire;
        }
    }
}
