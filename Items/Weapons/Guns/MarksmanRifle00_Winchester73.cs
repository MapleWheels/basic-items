﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Items;

namespace BasicItems.Items.Weapons.Guns
{
    /// <summary>
    /// This gun is 'inspired' by the Winchester 1873 as per the Defaults Name.
    /// It's intended to be the first Marksman Class weapon available to the
    /// player. The weapon uses a passive reload system, where 1 unit of ammo is
    /// loaded every X ticks. The player can fire faster than this using the
    /// weapon's internal storage of 15 rounds.
    /// </summary>
    /// <remarks>
    /// Uses a custom Item.UseTime tracking system due to a bug.
    /// </remarks>
    public class MarksmanRifle00_Winchester73 : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("Winchester '73");
            Tooltip.SetDefault("Chambered in Eastwood");
        }

        public override void SetDefaults()
        {
            item.damage = 11;
            item.crit = 5;
            item.ranged = true;
            item.width = 35;
            item.height = 16;
            item.useTime = 7;
            item.useAnimation = 6;
            item.useStyle = 5;
            item.noMelee = true;
            item.knockBack = 2f;
            item.value = 35000;
            item.rare = 2;
            //item.UseSound = SoundID.Item11;
            item.UseSound = mod.GetLegacySoundSlot(SoundType.Item, "Sounds/Item/Weapons/Guns/winchester73");
            item.autoReuse = false;
            item.shoot = mod.ProjectileType(nameof(Projectiles.MarksmanAmmo00_3840_Projectile));
            item.shootSpeed = 27f;
            item.useAmmo = mod.ItemType(nameof(Projectiles.MarksmanAmmo00_3840));
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                //Disabled, using NPC Shop
                //---Production Recipe---//
                //ModRecipe Production = new ModRecipe(mod);
                //Production.AddRecipeGroup("IronBar", 8);
                //Production.AddTile(TileID.Anvils);
                //Production.SetResult(this);
                //Production.AddRecipe();
            }
            else
            {
                //---Build Testing Recipe---//
                ModRecipe DevTesting = new ModRecipe(mod);
                DevTesting.SetResult(this);
                DevTesting.AddRecipe();
            }
        }

        

        public override Vector2? HoldoutOffset()
        {
            return new Vector2(-20, 0);
        }

        public override bool ConsumeAmmo(Player player)
        {
            return true;
            //return Main.rand.NextFloat() > 0f;
        }

        public override bool Shoot(Player player, ref Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)
        {
            Vector2 muzzleOffset = Vector2.Normalize(new Vector2(speedX, speedY) + new Vector2(0, -6)) * 28f;
            position += muzzleOffset;

            Vector2 perturbedSpeed = new Vector2(speedX, speedY).RotatedByRandom(MathHelper.ToRadians(3));
            speedX = perturbedSpeed.X;
            speedY = perturbedSpeed.Y;

            MarksmanRifle00_AmmoLoaded AmmoTracker = player.GetModPlayer<MarksmanRifle00_AmmoLoaded>(mod);
            if (!AmmoTracker.CanFire)
            {
                return false;
            }
            AmmoTracker.AmmoLoaded--;
            return true;
        }

        public override bool CanUseItem(Player player)
        {
            MarksmanRifle00_AmmoLoaded AmmoTracker = player.GetModPlayer<MarksmanRifle00_AmmoLoaded>(mod);
            return AmmoTracker.CanFire;
        }
    }
}
