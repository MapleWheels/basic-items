﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace BasicItems.Items.Weapons.Guns
{
    /// <summary>
    /// This class serves as the base for reloadable weapons. Guns should extend this class to implement a "cooldown reload" mechanic.
    /// If the player does not fire a round within the alloted timeframe, the gun is "reloaded"; CurrentAmmo is set to MagCapacity.
    /// </summary>
    public abstract class MarksmanRifle_ReloadableHandler : ModPlayer
    {
        public readonly int MagCapacity;    //How many rounds per magazine.
        public readonly int TimeToReloadTicks;   //How much idle time (no use) before the Magazine is replaced.

        private int _CurrentAmmoCount;
        public bool IsHeld { get; private set; }

        public virtual int CurrentAmmoCount     //Remaining ammunititon
        {
            get { return _CurrentAmmoCount; }
            set
            {
                _CurrentAmmoCount = Math.Min(MagCapacity, Math.Max(0, value));
            }
        }

        private int _TimeLeftBeforeReload;
        public virtual int TimeLeftBeforeReload //The remaining time before the Magazine is replaced.
        {
            get { return _TimeLeftBeforeReload; }
            private set
            {
                _TimeLeftBeforeReload = Math.Min(TimeToReloadTicks, Math.Max(0, value)); ;
            }
        }
        //helper bools
        public virtual bool CanReload { get { return TimeLeftBeforeReload <= 0; } }
        public virtual bool IsFullyLoaded { get { return CurrentAmmoCount == MagCapacity; } }
        public virtual bool CanFire { get { return CurrentAmmoCount > 0; } }

        public MarksmanRifle_ReloadableHandler(
            int MagCapacity = 1, 
            int TimeToReloadTicks = 1) : base()
        {
            //Defaults/placeholders, should be defined by subclass
            this.MagCapacity = MagCapacity;
            this.TimeToReloadTicks = TimeToReloadTicks;
        }

        public override void Initialize()
        {
            CurrentAmmoCount = MagCapacity;
            TimeLeftBeforeReload = TimeToReloadTicks;
        }

        public void ResetReloadTimer()
        {
            TimeLeftBeforeReload = TimeToReloadTicks;
        }

        public override void PostUpdate()
        {
            if (!IsFullyLoaded)
            {
                if (CanReload)
                {
                    ResetReloadTimer();
                    CurrentAmmoCount = MagCapacity;
                }
                else
                {
                    TimeLeftBeforeReload--;
                }
            }
        }
    }
}
