﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BasicItems.Items.Weapons.Guns
{
    /// <summary>
    /// This class implements the reload function for the "Stumgewehr 44" Assault Rifle Gun.
    /// </summary>
    public class MarksmanRifle03_M5E1_ReloadTracker : MarksmanRifle_ReloadableHandler
    {
        //base ( int <Mag Capacity>, int <Time To Reload In Ticks> )
        public MarksmanRifle03_M5E1_ReloadTracker() : base (20, (int)(4.5f*60f)) { } 
    }
}
