﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BasicItems.Items.Weapons.Guns
{
    /// <summary>
    /// This class handles the reload mechanic for the Winchester 1873.
    /// The player is able to shoot at a very high rate using the internal
    /// ammunition storage. However, the player is capped by the passive reload
    /// mechanic's loading rate. This allows the player to burst fire while
    /// clamping the maximum dps under continuous fire.
    /// </summary>
    /// <remarks>
    /// This class is to be turned into a generic that may be used with other weapons.
    /// </remarks>
    public class MarksmanRifle00_AmmoLoaded : ModPlayer
    {
        private int _AmmoLoaded;
        private int _TimeToNextLoad;
        public virtual int AmmoLoaded
        {
            get { return _AmmoLoaded; }
            set
            {
                _AmmoLoaded = Math.Max(0 ,Math.Min(value, AmmoCapacity));
            }
        }

        public readonly int loadTimePerRoundTicks = (int)(.7f * 60f); //To be replaced with constructor call
        public readonly int AmmoCapacity = 15;   //To be replaced with constructor call

        public virtual bool IsFullyLoaded
        {
            get { return (AmmoLoaded >= AmmoCapacity); }
        }

        public virtual bool LoadNextRound
        {
            get { return (_TimeToNextLoad <= 0); }
        }

        public virtual bool CanFire
        {
            get { return AmmoLoaded > 0; }
        }

        public override void Initialize()
        {
            AmmoLoaded = AmmoCapacity;
            _TimeToNextLoad = loadTimePerRoundTicks;
        }

        public override void PreUpdate()
        {
            if (!IsFullyLoaded)
            {
                if (LoadNextRound)
                {
                    _TimeToNextLoad = loadTimePerRoundTicks;
                    AmmoLoaded++;
                }
                else
                {
                    _TimeToNextLoad--;
                }
            }
        }
    }
}
