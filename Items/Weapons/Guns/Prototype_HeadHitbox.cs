﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BasicItems.Items.Weapons.Guns
{
    /// <summary>
    /// This class is for a prototype "headshot" mechanic. It serves the purpose of
    /// attempting to define a head hitbox and checking if the projectile has struct it.
    /// </summary>
    public class Prototype_HeadHitbox : ModPlayer
    {
        public override void OnHitNPCWithProj(Projectile proj, NPC target, int damage, float knockback, bool crit)
        {
            Vector2[] head = new Vector2[2];
            head[0] = target.position;  //top-left?
            //Assume for experiment head is top 1/4 of the sprite height
            head[1] = target.position + new Vector2(target.width, target.height/4);
           
            //Rectangle.Contains <= use it?
        }
    }
}
