﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Items;

namespace BasicItems.Items.Weapons.Guns
{
    public class MarksmanRifle02_Armalite15 : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("The American Staple");
            Tooltip.SetDefault("Chambered in .223");
        }

        public override void SetDefaults()
        {
            item.damage = 30;
            item.crit = 15;
            item.ranged = true;
            item.holdStyle = 3;
            item.width = 90;
            item.height = 30;
            item.useTime = 6; 
            item.useAnimation = 6;
            item.useStyle = 5;
            item.noMelee = true;
            item.knockBack = 8f;
            item.value = 275000;
            item.rare = 2;
            //item.UseSound = SoundID.Item11;
            item.UseSound = mod.GetLegacySoundSlot(SoundType.Item, "Sounds/Item/Weapons/Guns/ar15");
            item.autoReuse = true;
            item.shoot = mod.ProjectileType(nameof(Projectiles.MarksmanAmmo02_223_Projectile));
            item.shootSpeed = 36f;
            item.useAmmo = mod.ItemType(nameof(Projectiles.MarksmanAmmo02_223));
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                //Disabled, sold at ArmsDealer
                ////---Production Recipe---//
                //ModRecipe Production = new ModRecipe(mod);
                ////Production.AddIngredient(ItemID.Minishark, 1);
                //Production.AddRecipeGroup("IronBar", 3);
                ////Production.AddIngredient(ItemID.IllegalGunParts, 1);
                ////Production.AddIngredient(mod.ItemType("BasicGun"), 1); Disabled; dev mode only.
                //Production.AddTile(TileID.Anvils);
                //Production.SetResult(this);
                //Production.AddRecipe();
            }
            else
            {
                //---Build Testing Recipe---//
                ModRecipe DevTesting = new ModRecipe(mod);
                DevTesting.SetResult(this);
                DevTesting.AddRecipe();
            }
        }

        public override bool ConsumeAmmo(Player player)
        {
            return true;
            //return Main.rand.NextFloat() > 0f;
        }

        public override float MeleeSpeedMultiplier(Player player)
        {
            return this.UseTimeMultiplier(player);
        }

        public override Vector2? HoldoutOffset()
        {
            return new Vector2(-20, 0);
        }

        public override bool Shoot(Player player, ref Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)
        {
            Vector2 muzzleOffset = Vector2.Normalize(new Vector2(speedX, speedY) + new Vector2(0, -5)) * 23f;
            position += muzzleOffset;

            Vector2 perturbedSpeed = new Vector2(speedX, speedY).RotatedByRandom(MathHelper.ToRadians(1));
            speedX = perturbedSpeed.X;
            speedY = perturbedSpeed.Y;

            MarksmanRifle02_AR15_ReloadTracker AmmoTracker = player.GetModPlayer<MarksmanRifle02_AR15_ReloadTracker>(mod);
            if (!AmmoTracker.CanFire)
            {
                return false;
            }
            AmmoTracker.CurrentAmmoCount--;
            AmmoTracker.ResetReloadTimer();
            return base.Shoot(player, ref position, ref speedX, ref speedY, ref type, ref damage, ref knockBack);
        }

        public override bool CanUseItem(Player player)
        {
            MarksmanRifle02_AR15_ReloadTracker AmmoTracker = player.GetModPlayer<MarksmanRifle02_AR15_ReloadTracker>(mod);
            return AmmoTracker.CanFire;
        }
        
    }
}
