﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;
using Microsoft.Xna.Framework;

namespace BasicItems.Items.Weapons.Projectiles
{
    public class MarksmanAmmo00B_357 : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault(".357 Magnum");
            Tooltip.SetDefault("The Ugly.");
        }

        public override void SetDefaults()
        {
            item.damage = 11;
            item.crit = 4;
            item.width = 9;
            item.height = 9;
            item.maxStack = 999;
            item.consumable = true;
            item.knockBack = 2f;
            item.value = 2;
            item.rare = 8;
            item.shoot = mod.ProjectileType<MarksmanAmmo00_3840_Projectile>();
            item.ammo = mod.ItemType<MarksmanAmmo00_3840>();
        }

        public override void AddRecipes()
        {
            if (((Bootloader)mod).Cfg._bProductionMode)
            {
                //ModRecipe prod = new ModRecipe(mod);
                //prod.AddRecipeGroup("IronBar", 1);
                //prod.AddTile(TileID.Anvils);
                //prod.SetResult(this, 50);
                //prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this, item.maxStack);
                dev.AddRecipe();
            }
        }

        public override bool Shoot(Player player, ref Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)
        {
            return base.Shoot(player, ref position, ref speedX, ref speedY, ref type, ref damage, ref knockBack);
        }
    }
}
