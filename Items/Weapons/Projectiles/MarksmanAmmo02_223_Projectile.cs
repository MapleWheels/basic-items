﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BasicItems.Items.Weapons.Projectiles
{
    public class MarksmanAmmo02_223_Projectile : MarksmanProjectileCore
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Marksman_.223");
            base.SetStaticDefaults();
        }

        public override void SetDefaults()
        {
            projectile.width = 13;
            projectile.height = 13;
            projectile.penetrate = 1+PenetrationsByChance(55, 2);   //100% chance of 2 penetrations, 55% chance of 3;
            projectile.alpha = 0;
            base.SetDefaults();
        }

        public override void OnHitNPC(NPC target, int damage, float knockback, bool crit)
        {
            base.OnHitNPC(target, damage, knockback, crit);
        }

        public override void OnHitPlayer(Player target, int damage, bool crit)
        {
            base.OnHitPlayer(target, damage, crit);
        }

        public override void OnHitPvp(Player target, int damage, bool crit)
        {
            base.OnHitPvp(target, damage, crit);
        }
    }
}
