﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Weapons.Projectiles
{
    public class MarksmanAmmo00_3840 : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault(".38-40 LR");
            Tooltip.SetDefault("It's Duck Season.");
        }

        public override void SetDefaults()
        {
            item.damage = 7;
            item.crit = 2;
            item.width = 9;
            item.height = 9;
            item.maxStack = 999;
            item.consumable = true;
            item.knockBack = 2f;
            item.value = 2;
            item.rare = 8;
            item.shoot = mod.ProjectileType<MarksmanAmmo00_3840_Projectile>();
            item.ammo = mod.ItemType<MarksmanAmmo00_3840>();
        }

        public override void AddRecipes()
        {
            if (((Bootloader)mod).Cfg._bProductionMode)
            {
                //ModRecipe prod = new ModRecipe(mod);
                //prod.AddRecipeGroup("IronBar", 1);
                //prod.AddTile(TileID.Anvils);
                //prod.SetResult(this, 50);
                //prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this, item.maxStack);
                dev.AddRecipe();
            }
        }
    }
}
