﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Weapons.Projectiles
{
    public class MarksmanAmmo02_223 : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault(".223");
            Tooltip.SetDefault("NATO Reject");
        }

        public override void SetDefaults()
        {
            item.damage = 14;
            item.crit = 6;
            item.width = 9;
            item.height = 9;
            item.maxStack = 999;
            item.consumable = true;
            item.knockBack = 2f;
            item.value = 16;
            item.rare = 8;
            item.shoot = mod.ProjectileType<MarksmanAmmo02_223_Projectile>();
            item.ammo = mod.ItemType<MarksmanAmmo02_223>();
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                //ModRecipe prod = new ModRecipe(mod);
                //prod.AddRecipeGroup("IronBar", 1);
                //prod.AddTile(TileID.Anvils);
                //prod.SetResult(this, 50);
                //prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this, item.maxStack);
                dev.AddRecipe();
            }
        }
    }
}
