﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Weapons.Projectiles
{
    public class MarksmanAmmo03_308 : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault(".308 AP-T");
            Tooltip.SetDefault("Only the Right Stuff.");
        }

        public override void SetDefaults()
        {
            item.damage = 18;
            item.crit = 15;
            item.width = 7;
            item.height = 7;
            item.maxStack = 750;
            item.consumable = true;
            item.knockBack = 8f;
            item.value = 70;
            item.rare = 8;
            item.shoot = mod.ProjectileType<MarksmanAmmo03_308_Projectile>();
            item.ammo = mod.ItemType<MarksmanAmmo03_308>();
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                //ModRecipe prod = new ModRecipe(mod);
                //prod.AddRecipeGroup("IronBar", 1);
                //prod.AddTile(TileID.Anvils);
                //prod.SetResult(this, 50);
                //prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this, item.maxStack);
                dev.AddRecipe();
            }
        }
    }
}
