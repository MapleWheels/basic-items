﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;
using Microsoft.Xna.Framework;
using BasicItems.Items;

namespace BasicItems.Items.Weapons.Projectiles
{
    public abstract class MarksmanProjectileCore : ModProjectile
    {
        public bool HitValidEntity;

        public override void SetDefaults()
        {
            HitValidEntity = false;
            projectile.friendly = true;
            projectile.tileCollide = true;
            projectile.ignoreWater = true;
            projectile.ranged = true;
            projectile.timeLeft = 300;
        }

        public override bool PreKill(int timeLeft)
        {
            Equip.Marksman.Sigils.SigilEffectsHandler Source = Main.player[projectile.owner].GetModPlayer<Equip.Marksman.Sigils.SigilEffectsHandler>();

            if (HitValidEntity)
            {
                Source.CurrentStacks++;
                Source.ResetStackDecayTimer();
            }
            else
            {
                Source.CurrentStacks -= 2;   //Change if missed
                Source.ResetStackDecayTimer();
            }

            return true;
        }

        protected int PenetrationsByChance(int chance, int maxPenetrations)
        {
            if (maxPenetrations < 2) return 1;
            int t = 1;
            var r = new Random();
            for (int i = 0; i < maxPenetrations - 1; i++)
            {
                t += r.Next(0, 100) > chance ? 1 : 0;
            }
            return t;
        }

        public override void OnHitNPC(NPC target, int damage, float knockback, bool crit)
        {
            HitValidEntity = true;
        }

        public override void OnHitPlayer(Player target, int damage, bool crit)
        {
            HitValidEntity = true;
        }

        public override void OnHitPvp(Player target, int damage, bool crit)
        {
            HitValidEntity = true;
        }

        public override void AI()
        {
            //Main.NewText("ProjCore:AI()->HitValidEntity=" + HitValidEntity);
            Lighting.AddLight(projectile.Center, 0.8f, 0.7f, 0.02f);
            projectile.rotation = projectile.velocity.ToRotation();
        }
    }
}
