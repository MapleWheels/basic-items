﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BasicItems.Items.Weapons.Projectiles
{
    public class MarksmanAmmo01_8mm_Projectile : MarksmanProjectileCore
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Marksman_7.93mm");
            base.SetStaticDefaults();
        }

        public override void SetDefaults()
        {
            projectile.penetrate = PenetrationsByChance(70, 2); //70% chance to penetrate the target.
            projectile.width = 13;
            projectile.height = 13;
            projectile.alpha = 0;
            base.SetDefaults();
        }

        public override void OnHitNPC(NPC target, int damage, float knockback, bool crit)
        {
            base.OnHitNPC(target, damage, knockback, crit);
        }

        public override void OnHitPlayer(Player target, int damage, bool crit)
        {
            base.OnHitPlayer(target, damage, crit);
        }

        public override void OnHitPvp(Player target, int damage, bool crit)
        {
            base.OnHitPvp(target, damage, crit);
        }
    }
}
