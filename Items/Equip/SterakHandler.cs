﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Equip
{
    public class SterakHandler : ModPlayer
    {
        //----SHIELD VARS----//
        //Control and fixed vars
        public bool shieldEnabled;
        public bool shieldActive;
        public int shieldStrengthMax;
        public int shieldTiggerDmg;
        //Working vars
        private float shieldCurrentHP;
        private float shieldDecHPOverflow;  //Used to carry over decimal precision between ticks for HP decay
        private float shieldDecRate;
        private float shieldHealOverflow;   //Used to carry over decimal precision between ticks for Healing from shield
        //----END SHIELD----//
        public override void ResetEffects()
        {
            shieldEnabled = false;
        }

        public override bool Autoload(ref string name)
        {
            return true;
        }

        public override void Initialize()
        {
            shieldEnabled = false;
            shieldActive = false;
            shieldCurrentHP = 0.0f;
            shieldDecHPOverflow = 0.0f;
            shieldDecRate = 0.0f;
            base.Initialize();
        }

        public override bool PreHurt(bool pvp, bool quiet, ref int damage, ref int hitDirection, ref bool crit, ref bool customDamage, ref bool playSound, ref bool genGore, ref PlayerDeathReason damageSource)
        {
            if (shieldEnabled   //Shield is equipped
                && player.FindBuffIndex(mod.BuffType<SterakCooldown>()) == -1   //Sterak is not on cooldown
                && player.statLife < (int)(
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHpPercentDmgTriggerFactor * player.statLifeMax2)   //Health is low enough
                && damage > shieldTiggerDmg) //Damage is high enough to trigger
            {
                shieldDecRate = (float)shieldStrengthMax / ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldDecayTimeTicks;
                shieldCurrentHP = ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHpPercent * (float)player.statLifeMax2;
                shieldDecHPOverflow = 0.0f;
                shieldHealOverflow = 0.0f;

                Main.NewText(shieldCurrentHP.ToString(), 255, 255, 255, false);
                player.AddBuff(mod.BuffType<SterakCooldown>(), ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldCooldownTimeTicks);
                shieldActive = true;
                //initial, maxhp increased extra for one tick to allow statLife to not overflow.
                player.statLifeMax2 += (int)shieldCurrentHP;
                player.statLife += (int)shieldCurrentHP;
            }

            //Version 1
            //if (shieldActive)
            //{
            //    if (shieldCurrentHP > damage)
            //    {
            //        shieldCurrentHP -= damage;
            //        damage = 0;
            //    }
            //    else
            //    {
            //        damage -= (int)shieldCurrentHP;
            //        shieldCurrentHP = 0;
            //        shieldActive = false;
            //    }
            //}

            //Version 2: not tested yet (2019/01/16)
            if (shieldActive)
            {
                float diff;
                if ((shieldCurrentHP -= (diff = Math.Min(shieldCurrentHP, damage))) <= 0f)
                {
                    shieldActive = false;
                }
                damage -= (int)diff;
            }

            return base.PreHurt(pvp, quiet, ref damage, ref hitDirection, ref crit, ref customDamage, ref playSound, ref genGore, ref damageSource);
        }

        public override void PostUpdateBuffs()
        {
            //Doesn't seem to work in PreUpdate()
            if (shieldActive)
            {
                //Max HP handle
                shieldCurrentHP -= shieldDecRate;   //Subtract decay from shield
                player.statLifeMax2 += (int)shieldCurrentHP;    //Apply shield value to MaxHP each tick.
                //Current HP handle
                shieldDecHPOverflow += shieldDecRate;   //Decay is stored to keep track of decimal precision between ticks
                shieldHealOverflow += shieldDecRate * ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHeathConversionFactor;

                player.statLife -= (int)shieldDecHPOverflow;
                shieldDecHPOverflow -= (float)Math.Floor(shieldDecHPOverflow);

                player.statLife += (int)shieldHealOverflow;
                shieldHealOverflow -= (float)Math.Floor(shieldHealOverflow);

                if (shieldCurrentHP < 1f)
                {
                    shieldActive = false;
                }
            }
            base.PostUpdateBuffs();
        }


    }
}
