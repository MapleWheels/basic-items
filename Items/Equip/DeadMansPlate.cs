﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Equip
{
    public class DeadMansPlate : ModItem
    {
        //public static readonly float chargeRatePerTick =
        //     ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeRatePerTick;
        //public static readonly float chargeToSpeedRatio =
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeToSpeedFactor;
        //public static readonly float chargeToDefenseRatio =
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeToDefenseFactor;
        //public static readonly float chargeToDamagePercentRatio =
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeToDamageFactor;
        //public static readonly int maxCharges =
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.MaxCharges;

        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Dead Man's Plate");
            Tooltip.SetDefault("Gain charges while moving, charges give move speed and defense. " +
                "Your next attack will convert all charges into up to " + 
                (((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeToDamageFactor * 100f) + "% bonus damage.");
        }

        public override void SetDefaults()
        {
            item.width = 30;
            item.height = 28;
            item.value = 10000;
            item.rare = 2;
            item.accessory = true;
            item.defense = ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.PassiveDefense;
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            DeadMansPlateHandler dmph = player.GetModPlayer<DeadMansPlateHandler>(mod);
            dmph.plateEnabled = true;
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                ModRecipe recipe = new ModRecipe(mod);
                recipe.AddIngredient(ItemID.ObsidianShield, 1);
                recipe.AddIngredient(ItemID.EoCShield, 1);
                recipe.AddTile(TileID.WorkBenches);
                recipe.SetResult(this);
                recipe.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }
}
