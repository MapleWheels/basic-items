﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Equip
{
    public class SterakGauge : ModItem
    {
        //public static readonly float shieldHPPercent =
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHpPercent;
        //public static readonly int shieldDecayTime =      //10 * 60;    //60 ticks per second
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldDecayTimeTicks;
        //public static readonly float shieldHPTrigger = 0.5f;
        //public static readonly int shieldCooldown = 120 * 60;
        //public static readonly float shieldhealthConversion = 0.5f;
        //public static readonly float shieldHPPercentDmgTrigger = 0.2f;    

        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Sterak's Guage");
            Tooltip.SetDefault(string.Format("Grants a decaying shield for {0}% of max hp,\n" +
                "when below {1}%hp and more than {2}(capped: 200) damage is taken.\n" +
                "{3}% of the decaying shield will turn back into health.",
                 ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHpPercent * 100.0f,
                 ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHpPercent * 100.0f,
                 ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge .ShieldHpPercentDmgTriggerFactor* 100.0f,
                 ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHeathConversionFactor * 100.0f
                 ));
        }

        public override void SetDefaults()
        {
            //item.name = "Sterak's Guage"; --OLD Modloader
            item.width = 30;
            item.height = 28;
            item.value = 10000;
            item.rare = 2;
            item.accessory = true;
            //item.toolTip = "Anti-Burst Protector, Try Me."; //Needs to be set in Static Defaults --OLD Modloader
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            //Base stat boosts
            player.statLifeMax2 += 30;
            //Setup handling
            int shieldMaxHP = (int)Math.Round((float)player.statLifeMax 
                * ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHpPercent);
            SterakHandler ph = player.GetModPlayer<SterakHandler>(mod);
            ph.shieldEnabled = true;
            ph.shieldStrengthMax = shieldMaxHP;
            ph.shieldTiggerDmg = (int)Math.Round((float)player.statLifeMax 
                * ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.SterakGauge.ShieldHpPercentDmgTriggerFactor);
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                ModRecipe recipe = new ModRecipe(mod);
                recipe.AddIngredient(ItemID.PanicNecklace, 1);
                recipe.AddIngredient(ItemID.ObsidianShield, 1);
                recipe.AddTile(TileID.WorkBenches);
                recipe.SetResult(this);
                recipe.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }
}
