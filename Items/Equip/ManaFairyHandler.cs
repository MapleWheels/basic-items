﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Equip
{
    public class ManaFairyHandler : ModPlayer
    {
        public bool manaFairyEnabled;

        public override bool Autoload(ref string name)
        {
            return true;
        }

        public override void ResetEffects()
        {
            manaFairyEnabled = false;
            base.ResetEffects();
        }

        public override bool PreHurt(bool pvp, bool quiet, ref int damage, ref int hitDirection, ref bool crit, ref bool customDamage, ref bool playSound, ref bool genGore, ref PlayerDeathReason damageSource)
        {
            if (manaFairyEnabled)
            {
                //Calculate % damage mitigation; verbose for legibility.
                float w, x, y, z, r, dmgMitigationActual;
                w = (float)player.statLife/player.statLifeMax2;     //.............................% Max HP

                x = 1f - ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaMinHP;       //............% hp range to cover
                y = ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaRatioMax 
                    - ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaRatioMin;    //...............% damage conversion scaling range
                z = (y / x);    //.................................................................% units of damage reduction per unit of missing %hp

                r = (1 - w) * z + ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaRatioMin;   //....% Damage conversion actual.

                dmgMitigationActual = Math.Min(damage * r, player.statMana);    //..........Mitigation limited to mana pool
                damage -= (int)dmgMitigationActual;     //..................................Reduce incoming damage
                player.statMana -= (int)dmgMitigationActual;        //......................Sub cost from mana pool
            }

            return base.PreHurt(pvp, quiet, ref damage, ref hitDirection, ref crit, ref customDamage, ref playSound, ref genGore, ref damageSource);
        }
    }
}
