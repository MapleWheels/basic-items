﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using BasicItems.Core;

namespace BasicItems.Items.Equip.Marksman.Sigils
{
    public class Rank02 : ModItem
    {
        public const int SigilRank = 2;

        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Sharpshooter Sigil: Rank " + SigilRank);
            Tooltip.SetDefault(String.Format(
                "Perks: \n Trigger Happy: \n" +
                "Each Successful hit on an enemy grants 1 stack, up to {0} stacks.\n" +
                "Each stack grants +{1} Flat Damage, {2}% Total Damage, -{3} Flat Target Armor,\n" +
                "Target Armor Reduced to {4}%, +{5}% Flat Critical Chance and {6}% Total Crit Chance.",
                Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.MaxStacks,
                Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.DamageBonusFlatPerStack,
                (Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.DamageBonusFactorPerStack - 1f) * 100f,
                Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.DefenseReductionFlatPerStack,
                (Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.DefenseReductionFactorPerStack - 1f) * 100f,
                Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.CritBonusFlatPerStack,
                (Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.CritBonusFactorPerStack - 1f) * 100f
                ));
            base.SetStaticDefaults();
        }

        public override void SetDefaults()
        {
            item.width = 30;
            item.height = 28;
            item.value = 1000;
            item.rare = 2;
            item.accessory = true;
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            SigilEffectsHandler sigilEffectsHandler = player.GetModPlayer<SigilEffectsHandler>(mod);

            //This reduces unnecessary value assignments every tick
            if (sigilEffectsHandler.Configured <= 0)
            {
                sigilEffectsHandler.Configured = 2; //One extra in count allows for tick overlap

                sigilEffectsHandler.ActiveEffects.DamageBonusFlatPerStack =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.DamageBonusFlatPerStack;
                sigilEffectsHandler.ActiveEffects.DamageBonusFactorPerStack =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.DamageBonusFactorPerStack;
                sigilEffectsHandler.ActiveEffects.CritBonusFlatPerStack =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.CritBonusFlatPerStack;
                sigilEffectsHandler.ActiveEffects.CritBonusFactorPerStack =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.CritBonusFactorPerStack;
                sigilEffectsHandler.ActiveEffects.DefenseReductionFlatPerStack =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.DefenseReductionFlatPerStack;
                sigilEffectsHandler.ActiveEffects.DefenseReductionFactorPerStack =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.DefenseReductionFactorPerStack;
                sigilEffectsHandler.ActiveEffects.MaxStacks =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.MaxStacks;
                sigilEffectsHandler.ActiveEffects.Ichor =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.Ichor;
                sigilEffectsHandler.ActiveEffects.Fire =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.Fire;
                sigilEffectsHandler.ActiveEffects.FrostBurn =
                    Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.Rank02.FrostBurn;
            }
            else
            {
                sigilEffectsHandler.Configured++;
            }
        }

        public override void AddRecipes()
        {
            if (Bootloader.Instance.Cfg._bProductionMode)
            {
                ModRecipe prod = new Core.Util.ModRecipeHelper(mod)
                {
                    OverrideRecipeAvailable = () => { return NPC.downedBoss2; }
                };
                prod.AddIngredient(mod.GetItem<Rank01>(), 1);
                prod.AddTile(TileID.Anvils);
                prod.SetResult(this);
                prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }
}
