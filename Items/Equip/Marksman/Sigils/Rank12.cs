﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using BasicItems.Core;

namespace BasicItems.Items.Equip.Marksman.Sigils
{
    public class Rank12 : ModItem
    {
        public const int SigilRank = 12;

        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Sharpshooter Sigil: Rank " + SigilRank);
            Tooltip.SetDefault(String.Format(
                "Perks: \n Trigger Happy: \n" +
                "Each Successful hit on an enemy grants 1 stack, up to {0} stacks.\n" +
                "Each stack grants +{1} Flat Damage, {2}% Total Damage, -{3} Flat Target Armor,\n" +
                "Target Armor Reduced to {4}%, +{5}% Flat Critical Chance and {6}% Total Crit Chance.",
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.MaxStacks,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.DamageBonusFlatPerStack,
                (((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.DamageBonusFactorPerStack - 1f) * 100f,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.DefenseReductionFlatPerStack,
                (((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.DefenseReductionFactorPerStack - 1f) * 100f,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.CritBonusFlatPerStack,
                (((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.CritBonusFactorPerStack - 1f) * 100f
                ));
            base.SetStaticDefaults();
        }

        public override void SetDefaults()
        {
            item.width = 30;
            item.height = 28;
            item.value = 1000;
            item.rare = 2;
            item.accessory = true;
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            SigilEffectsHandler sigilEffectsHandler = player.GetModPlayer<SigilEffectsHandler>(mod);

            //This reduces unnecessary value assignments every tick
            if (sigilEffectsHandler.Configured <= 0)
            {
                sigilEffectsHandler.Configured = 2; //One extra in count allows for tick overlap

                sigilEffectsHandler.ActiveEffects.DamageBonusFlatPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.DamageBonusFlatPerStack;
                sigilEffectsHandler.ActiveEffects.DamageBonusFactorPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.DamageBonusFactorPerStack;
                sigilEffectsHandler.ActiveEffects.CritBonusFlatPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.CritBonusFlatPerStack;
                sigilEffectsHandler.ActiveEffects.CritBonusFactorPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.CritBonusFactorPerStack;
                sigilEffectsHandler.ActiveEffects.DefenseReductionFlatPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.DefenseReductionFlatPerStack;
                sigilEffectsHandler.ActiveEffects.DefenseReductionFactorPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.DefenseReductionFactorPerStack;
                sigilEffectsHandler.ActiveEffects.MaxStacks =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.MaxStacks;
                sigilEffectsHandler.ActiveEffects.Ichor =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.Ichor;
                sigilEffectsHandler.ActiveEffects.Fire =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.Fire;
                sigilEffectsHandler.ActiveEffects.FrostBurn =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank12.FrostBurn;
            }
            else
            {
                sigilEffectsHandler.Configured++;
            }
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                ModRecipe prod = new Core.Util.ModRecipeHelper(mod)
                {
                    OverrideRecipeAvailable = () => { return NPC.downedBoss3; }
                };
                prod.AddIngredient(mod.GetItem<Rank11>(), 1);
                prod.AddTile(TileID.Anvils);
                prod.SetResult(this);
                prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }
}
