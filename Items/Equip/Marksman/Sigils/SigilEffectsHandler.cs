﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Equip.Marksman.Sigils
{
    /// <summary>
    /// This struct keeps the values of the current Sigil equipped by the player.
    /// </summary>
    public struct EnabledSigilEffects
    {
        public int DamageBonusFlatPerStack;
        public float DamageBonusFactorPerStack;
        public int CritBonusFlatPerStack;
        public float CritBonusFactorPerStack;
        public int DefenseReductionFlatPerStack;
        public float DefenseReductionFactorPerStack;
        public int MaxStacks;
        //Status effects
        public bool Ichor;
        public bool Fire;
        public bool FrostBurn;
    }

    public class SigilEffectsHandler : ModPlayer
    {
        public Config config;
        public EnabledSigilEffects ActiveEffects;
        public int Configured;

        private int _CurrentStacks;
        public virtual int CurrentStacks
        {
            get { return _CurrentStacks; }
            set
            {
                _CurrentStacks = Math.Min(Math.Max(0, value), ActiveEffects.MaxStacks);
            }
        }

        public virtual bool AtMaxStacks
        {
            get { return CurrentStacks == ActiveEffects.MaxStacks; }
        }

        public virtual bool Enabled {
            get { return (Configured > 0); }
        }

        public int StackDecayTimeTicks { get; private set; }

        public SigilEffectsHandler()
        {
            ActiveEffects = new EnabledSigilEffects();
        }

        public void ResetStackDecayTimer()
        {
            StackDecayTimeTicks = Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.StackDecayTimeTicks;
        }

        public override void Initialize()
        {
            config = Bootloader.Instance.Cfg;
            ActiveEffects = new EnabledSigilEffects();
            Configured = 0;
            CurrentStacks = 0;
            StackDecayTimeTicks = Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.StackDecayTimeTicks;
        }

        public override void PreUpdate()
        {
            if (Enabled)
            {
                if (CurrentStacks > 0)
                {
                    if (StackDecayTimeTicks > 0)
                    {
                        StackDecayTimeTicks--;
                    }
                    else
                    {
                        StackDecayTimeTicks = Bootloader.Instance.Cfg.Flags.Items.Equip.Marksman.Sigils.StackDecayTimeTicks;
                        CurrentStacks--;
                    }
                }
                Configured--;   //if this hits 0 then the ModItem class will reassign ActiveEffects vars, 
                                //then set this to 2. Avoids unnecessary updates.
                player.rangedCrit *= (int)Math.Pow(
                    ActiveEffects.CritBonusFactorPerStack, CurrentStacks);
                player.rangedCrit += ActiveEffects.CritBonusFlatPerStack * CurrentStacks;
            }
            else
            {
                CurrentStacks = 0;
            }
        }

        public override bool Autoload(ref string name)
        {
            return true;
        }

        public override void OnHitNPCWithProj(Projectile proj, NPC target, int damage, float knockback, bool crit)
        {
            if (proj.modProjectile is Weapons.Projectiles.MarksmanProjectileCore)
            {
                target.defense *= (int)Math.Pow(ActiveEffects.DefenseReductionFactorPerStack, CurrentStacks);
                target.defense -= ActiveEffects.DefenseReductionFlatPerStack * CurrentStacks;

                if (AtMaxStacks)
                {
                    if (ActiveEffects.Ichor)
                        target.AddBuff(BuffID.Ichor, 2 * 60);
                    if (ActiveEffects.FrostBurn)
                        target.AddBuff(BuffID.Frostburn, 2 * 60);
                    if (ActiveEffects.Fire)
                        target.AddBuff(BuffID.OnFire, 2 * 60);
                }
            }
        }

        public override void GetWeaponDamage(Item item, ref int damage)
        {
            damage += ActiveEffects.DamageBonusFlatPerStack * CurrentStacks;
            damage = (int)Math.Round(damage * Math.Pow(
                ActiveEffects.DamageBonusFactorPerStack, CurrentStacks));
            //Calamity Balance Factor
            damage = (int)Math.Round(damage * _Flags._iCalFactor);
        }

        public override void OnHitPvpWithProj(Projectile proj, Player target, int damage, bool crit)
        {
            if (proj.modProjectile is Weapons.Projectiles.MarksmanProjectileCore)
            {
                target.statDefense *= (int)Math.Pow(ActiveEffects.DefenseReductionFactorPerStack, CurrentStacks);
                target.statDefense -= ActiveEffects.DefenseReductionFlatPerStack * CurrentStacks;
            }
        }
    }
}
