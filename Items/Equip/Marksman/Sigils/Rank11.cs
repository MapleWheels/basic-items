﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using BasicItems.Core;

namespace BasicItems.Items.Equip.Marksman.Sigils
{
    public class Rank11 : ModItem
    {
        public const int SigilRank = 11;

        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Sharpshooter Sigil: Rank " + SigilRank);
            Tooltip.SetDefault(String.Format(
                "Perks: \n Trigger Happy: \n" +
                "Each Successful hit on an enemy grants 1 stack, up to {0} stacks.\n" +
                "Each stack grants +{1} Flat Damage, {2}% Total Damage, -{3} Flat Target Armor,\n" +
                "Target Armor Reduced to {4}%, +{5}% Flat Critical Chance and {6}% Total Crit Chance.",
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.MaxStacks,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.DamageBonusFlatPerStack,
                (((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.DamageBonusFactorPerStack - 1f) * 100f,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.DefenseReductionFlatPerStack,
                (((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.DefenseReductionFactorPerStack - 1f) * 100f,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.CritBonusFlatPerStack,
                (((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.CritBonusFactorPerStack - 1f) * 100f
                ));
            base.SetStaticDefaults();
        }

        public override void SetDefaults()
        {
            item.width = 30;
            item.height = 28;
            item.value = 1000;
            item.rare = 2;
            item.accessory = true;
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            SigilEffectsHandler sigilEffectsHandler = player.GetModPlayer<SigilEffectsHandler>(mod);

            //This reduces unnecessary value assignments every tick
            if (sigilEffectsHandler.Configured <= 0)
            {
                sigilEffectsHandler.Configured = 2; //One extra in count allows for tick overlap

                sigilEffectsHandler.ActiveEffects.DamageBonusFlatPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.DamageBonusFlatPerStack;
                sigilEffectsHandler.ActiveEffects.DamageBonusFactorPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.DamageBonusFactorPerStack;
                sigilEffectsHandler.ActiveEffects.CritBonusFlatPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.CritBonusFlatPerStack;
                sigilEffectsHandler.ActiveEffects.CritBonusFactorPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.CritBonusFactorPerStack;
                sigilEffectsHandler.ActiveEffects.DefenseReductionFlatPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.DefenseReductionFlatPerStack;
                sigilEffectsHandler.ActiveEffects.DefenseReductionFactorPerStack =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.DefenseReductionFactorPerStack;
                sigilEffectsHandler.ActiveEffects.MaxStacks =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.MaxStacks;
                sigilEffectsHandler.ActiveEffects.Ichor =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.Ichor;
                sigilEffectsHandler.ActiveEffects.Fire =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.Fire;
                sigilEffectsHandler.ActiveEffects.FrostBurn =
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.Marksman.Sigils.Rank11.FrostBurn;
            }
            else
            {
                sigilEffectsHandler.Configured++;
            }
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                ModRecipe prod = new Core.Util.ModRecipeHelper(mod)
                {
                    OverrideRecipeAvailable = () => { return NPC.downedAncientCultist; }
                };
                prod.AddIngredient(mod.GetItem<Rank10>(), 1);
                prod.AddTile(TileID.Anvils);
                prod.SetResult(this);
                prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }
}
