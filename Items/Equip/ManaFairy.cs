﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Equip
{
    public class ManaFairy : ModItem
    {
        //public static readonly float damageToManaRatioMin = 
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaRatioMin;
        //public static readonly float damageToManaRatioMax = 
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaRatioMax;
        //public static readonly float damageToManaMinHP = 
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaMinHP;
        //public static readonly int playerManaBoost = 
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.PlayerManaBonus;
        //public static readonly float magicDamageBonusRatio = 
        //    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.MagicDamageBonusRatio;

        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("Mana Fairy");
            Tooltip.SetDefault(string.Format("A kind ally attracted to those with high potential.\n"
                +"Increases maximum mana by {2}.\n"
                +"{0}% to {1}% of incoming damage will be taken from mana instead, based on missing hp.\n"
                + "Increase magic damage by {3}% of your current mana (not max).",
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaRatioMin*100.0f,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.DamageToManaRatioMax*100.0f,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.PlayerManaBonus,
                ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.MagicDamageBonusRatio*100f));
        }

        public override void SetDefaults()
        {
            item.accessory = true;
            item.width = 30;
            item.height = 28;
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            ManaFairyHandler mfh = player.GetModPlayer<ManaFairyHandler>(mod);
            mfh.manaFairyEnabled = true;
            player.statManaMax2 += ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.PlayerManaBonus;

            player.magicDamage *= (1f + ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.ManaFairy.MagicDamageBonusRatio * 
                (float)player.statMana / player.statLifeMax2);
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                ModRecipe mr = new ModRecipe(mod);
                mr.AddIngredient(ItemID.DirtBlock, 2);
                mr.AddTile(TileID.WorkBenches);
                mr.SetResult(this);
                mr.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }
}
