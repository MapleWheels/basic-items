﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BasicItems.Items.Equip
{
    public class SterakCooldown : ModBuff
    {
         

        public override void SetDefaults()
        {
            //Main.buff[Type] = "Sterak Overheated!";
            //Main.buffTip[Type] = "Sterak needs to cooldown before it can activate again";
            Main.debuff[Type] = true;
            Main.buffNoSave[Type] = true;
        }

        //Stop vanilla Terraria reapply code
        public override bool ReApply(Player player, int time, int buffIndex)
        {
            return true;
        }
        //Same as above but also stops healer from removing it
        public override bool ReApply(NPC npc, int time, int buffIndex)
        {
            return true;
        }
    }
}
