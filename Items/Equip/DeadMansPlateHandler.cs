﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Equip
{
    public class DeadMansPlateHandler : ModPlayer
    {
        //Control vars
        public bool plateEnabled;
        //Working vars
        private float _charges;
        public virtual float Charges {
            get { return _charges; }
            private set { _charges = 
                    Math.Max(0f, Math.Min(
                            value, ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.MaxCharges
                            ));
            }
        }

        public override void ResetEffects()
        {
            plateEnabled = false;
            base.ResetEffects();
        }

        public override void Initialize()
        {
            Charges = 0.0f;
            base.Initialize();
        }

        public override void PostUpdateRunSpeeds()
        {
            if (plateEnabled) {
                if ((player.velocity.X != 0 || player.velocity.Y != 0)
                    && Charges < ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.MaxCharges)
                {
                    Charges += ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeRatePerTick;
                }

                player.statDefense += (int)(Charges * ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeToDefenseFactor);
                player.maxRunSpeed += Charges * ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeToSpeedFactor;    //Gives the speed to ram
            }
            else if (!plateEnabled)
            {
                Charges = 0.0f;
            }
        }

        public override void GetWeaponDamage(Item item, ref int damage)
        {
            if (plateEnabled)
            {
                damage += (int)Math.Round( damage * (
                    (Charges/(float)((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.MaxCharges) *
                    ((Core.Bootloader)mod).Cfg.Flags.Items.Equip.DeadMansPlate.ChargeToDamageFactor)
                    );
            }
        }

        public override void OnHitAnything(float x, float y, Entity victim)
        {
            Charges = 0.0f;
            base.OnHitAnything(x, y, victim);
        }
    }
}
