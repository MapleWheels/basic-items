﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.BasicWeapons
{
    class ShitHook : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("Shit hook");
            Tooltip.SetDefault("If you let go you'll end up in the shitter.");
        }

        public override void SetDefaults()
        {
            item.CloneDefaults(ItemID.AmethystHook);
            //item.name = "Shit Hook";
            //item.toolTip = "If you let go you'll end up in the shitter.";
            item.shoot = mod.ProjectileType("ShitHookProjectile");
            item.shootSpeed = 35f;
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                ////Disabled; dev mode only
                //ModRecipe prod = new ModRecipe(mod);
                //prod.AddRecipeGroup("IronBar", 10);
                //prod.AddTile(TileID.Anvils);
                //prod.SetResult(this);
                //prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }

    class ShitHookProjectile : ModProjectile
    {
        const int maxhooks = 3;

        public override void SetDefaults()
        {
            projectile.CloneDefaults(ProjectileID.GemHookAmethyst);
        }

        public override bool? CanUseGrapple(Player player)
        {
            int hooksOut = 0;
            for (int l = 0; l < 1000; l++)
            {
                if (Main.projectile[l].active && Main.projectile[l].owner == Main.myPlayer && Main.projectile[l].type == projectile.type)
                {
                    hooksOut++;
                }
            }
            if (hooksOut > (maxhooks-1)) 
            {
                return false;
            }
            return true;
        }

        public override float GrappleRange()
        {
            return 300f;
        }

        public override void NumGrappleHooks(Player player, ref int numHooks)
        {
            numHooks = maxhooks-1;
        }

        public override void GrappleRetreatSpeed(Player player, ref float speed)
        {
            speed = 25f;
        }

        public override bool PreDraw(SpriteBatch spriteBatch, Color lightColor)
        {
            Vector2 mountedCenter = Main.player[projectile.owner].MountedCenter;
            Vector2 vector14 = new Vector2(projectile.position.X + (float)projectile.width * 0.5f, projectile.position.Y + (float)projectile.height * 0.5f);
            float num84 = mountedCenter.X - vector14.X;
            float num85 = mountedCenter.Y - vector14.Y;
            float rotation13 = (float)Math.Atan2((double)num85, (double)num84) - 1.57f;
            bool flag11 = true;
            while (flag11)
            {
                float num86 = (float)Math.Sqrt((double)(num84 * num84 + num85 * num85));
                if (num86 < 30f)
                {
                    flag11 = false;
                }
                else if (float.IsNaN(num86))
                {
                    flag11 = false;
                }
                else
                {
                    num86 = 24f / num86;
                    num84 *= num86;
                    num85 *= num86;
                    vector14.X += num84;
                    vector14.Y += num85;
                    num84 = mountedCenter.X - vector14.X;
                    num85 = mountedCenter.Y - vector14.Y;
                    Color color15 = Lighting.GetColor((int)vector14.X / 16, (int)(vector14.Y / 16f));
                    try
                    {
                        Main.spriteBatch.Draw(mod.GetTexture("ShitHookChain"), 
                            new Vector2(vector14.X - Main.screenPosition.X, vector14.Y - Main.screenPosition.Y), 
                            new Microsoft.Xna.Framework.Rectangle?(new Rectangle(0, 0, Main.chain30Texture.Width, Main.chain30Texture.Height)), 
                            color15, rotation13, new Vector2(Main.chain30Texture.Width * 0.5f,
                            Main.chain30Texture.Height * 0.5f), 1f, SpriteEffects.None, 0f);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    
                }
            }
            return true;
        }
    }
}
