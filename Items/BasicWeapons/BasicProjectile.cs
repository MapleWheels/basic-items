﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BasicItems.Items.BasicWeapons
{
    public class BasicProjectile : ModProjectile
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("BasicProjectile");
        }

        public override void SetDefaults()
        {
            //projectile.name = "BasicProjectile";
            projectile.width = 14;
            projectile.height = 14;
            projectile.alpha = 0;
            projectile.friendly = true;
            projectile.tileCollide = true;
            projectile.ignoreWater = true;
            projectile.ranged = true;
            ProjectileID.Sets.Homing[projectile.type] = true;
        }


    }
}
