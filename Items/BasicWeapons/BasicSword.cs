﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.BasicWeapons
{
    public class BasicSword : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("Basic Sword");
            Tooltip.SetDefault("swoosh");
        }

        public override void SetDefaults()
        {
            //item.name = "Basic Sword";
            item.damage = 20;
            item.melee = true;
            item.width = 32;
            item.height = 32;
            //item.toolTip = "It says basic but...";
            item.useTime = 6;
            item.useAnimation = 6;
            item.useStyle = 1;
            item.knockBack = 2;
            item.value = 10;
            item.rare = 2;
            item.crit = 20;
            item.UseSound = SoundID.Item1;
            item.autoReuse = true;
            item.useTurn = true;
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                ////Disabled; dev mode only
                //ModRecipe recipe = new ModRecipe(mod);
                //recipe.AddIngredient(ItemID.DirtBlock, 10);
                //recipe.AddTile(TileID.WorkBenches);
                //recipe.SetResult(this);
                //recipe.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }

        public override void MeleeEffects(Player player, Rectangle hitbox)
        {
            if (Main.rand.Next(3) == 0)
            {
                int dust = Dust.NewDust(new Vector2(hitbox.X, hitbox.Y), hitbox.Width, hitbox.Height, mod.DustType("Sparkle"));
            }
        }

        //Enable once recipe is balanced
        //public override void OnHitNPC(Player player, NPC target, int damage, float knockBack, bool crit)
        //{
        //    target.AddBuff(BuffID.OnFire, 60);
        //    target.AddBuff(BuffID.Ichor, 20);
        //}
    }
}
