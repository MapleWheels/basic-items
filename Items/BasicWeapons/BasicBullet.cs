﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.BasicWeapons
{
    public class BasicBullet : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("Basic Bullet");
            Tooltip.SetDefault("Zoom.");
        }

        public override void SetDefaults()
        {
            //item.name = "Basic Bullet";
            item.damage = 7;
            item.crit = 15;
            item.width = 14;
            item.height = 14;
            item.maxStack = 500;
            //item.toolTip = "makes pew";
            item.consumable = true;
            item.knockBack = 2f;
            item.value = 6;
            item.rare = 8;
            item.shoot = mod.ProjectileType("BasicProjectile");
            item.ammo = item.type;
        }

        public override void AddRecipes()
        {
            if (((Core.Bootloader)mod).Cfg._bProductionMode)
            {
                ////Disabled; dev use only
                //BasicBulletRecipe prod = new BasicBulletRecipe(mod);
                //prod.AddRecipeGroup("IronBar", 1);
                //prod.AddIngredient(ItemID.ExplosivePowder, 1);
                //prod.AddTile(TileID.Anvils);
                //prod.SetResult(this, 50);
                //prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this, 50);
                dev.AddRecipe();
            }
        }
    }

    public class BasicBulletRecipe : ModRecipe
    {
        public BasicBulletRecipe(Mod mod) : base (mod)
        {

        }

        public override bool RecipeAvailable()
        {
            return (
                Main.LocalPlayer.HasItem(mod.ItemType("BasicGun")) ||
                Main.LocalPlayer.HasItem(mod.ItemType("IlluminatiPumperMachineGun"))
                );
        }

        public override int ConsumeItem(int type, int numRequired)
        {
            return base.ConsumeItem(type, numRequired);
        }
    }
}
