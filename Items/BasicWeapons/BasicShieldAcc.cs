﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using BasicItems.Core;

namespace BasicItems.Items.BasicWeapons
{
    public class BasicShieldAcc : ModItem 
    {

        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("Basic Shield");
            Tooltip.SetDefault("ding");
        }

        public override void SetDefaults()
        {
            item.width = 30;
            item.height = 28;
            item.value = 1000;
            item.rare = 2;
            item.accessory = true;
            item.defense = 10;
            item.lifeRegen = 15;
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            player.meleeDamage += 2f;
            player.endurance -= 0.1f;
            player.statLifeMax2 += 40;
        }

        public override void AddRecipes()
        {
            if (((Bootloader)mod).Cfg._bProductionMode)
            {
                ////Disabled; dev mode only
                //ModRecipe prod = new ModRecipe(mod);
                //prod.AddIngredient(ItemID.DirtBlock, 5);
                //prod.AddTile(TileID.WorkBenches);
                //prod.SetResult(this);
                //prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }
}
