﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.BasicWeapons
{
    public class BasicGun : ModItem  
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("Basic Gun: Tier 1");
            Tooltip.SetDefault("Zooms with the power of [Confidential].");
        }

        public override void SetDefaults()
        {
            //item.name = "Basic Gun";
            item.damage = 12;
            item.crit = 5;
            item.ranged = true;
            item.width = 45;
            item.height = 15;
            //item.toolTip = "pew";
            item.useTime = 8;
            item.useAnimation = 2;
            item.useStyle = 5;
            item.noMelee = true;
            item.knockBack = 4f;
            item.value = 100;
            item.rare = 2;
            item.UseSound = SoundID.Item11;
            item.autoReuse = true;
            item.shoot = mod.ProjectileType("BasicProjectile");
            item.shootSpeed = 25f;
            item.useAmmo = mod.ItemType("BasicBullet");
        }

        public override void AddRecipes()
        {
            if (((Bootloader)mod).Cfg._bProductionMode)
            {
                ////Disabled; dev mode only
                //ModRecipe prod = new ModRecipe(mod);
                //prod.AddRecipeGroup("IronBar");
                //prod.AddTile(TileID.Anvils);
                //prod.SetResult(this);
                //prod.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }

        public override bool ConsumeAmmo(Player player)
        {
            return true;
            //return Main.rand.NextFloat() > 0f;
        }

        public override bool Shoot(Player player, ref Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)
        {
            Vector2 perturbedSpeed = new Vector2(speedX, speedY).RotatedByRandom(MathHelper.ToRadians(2));
            speedX = perturbedSpeed.X;
            speedY = perturbedSpeed.Y;
            return true;
        }


    }
}
