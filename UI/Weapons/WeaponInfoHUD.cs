﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria.UI;
using Terraria.Localization;
using Terraria.ModLoader;
using Terraria.GameContent;
using Terraria.GameContent.UI;
using Terraria.GameContent.UI.Elements;
using Terraria.GameContent.UI.States;
using Terraria;
using Microsoft.Xna.Framework;

namespace BasicItems.UI.Weapons
{
    internal class WeaponInfoHUD : UIState
    {
        public static bool Visible;
        public WeaponAmmoUIComponent weaponAmmoUIComponent;

        public override void OnInitialize()
        {
            Visible = true;
            UIPanel parent = new UIPanel();
            parent.Height.Set(100f, 0f);
            parent.Width.Set(200f, 0f);
            parent.Left.Set(Main.screenWidth - parent.Width.Pixels - 10, 0f);
            parent.Top.Set(Main.screenHeight - parent.Height.Pixels - 10, 0f);
            parent.BackgroundColor = new Color(255,255,255,255);

            base.Append(parent);
        }

        public class WeaponAmmoUIComponent : UIElement
        {
            public int currentAmmo;
            public int maxAmmo;

            public WeaponAmmoUIComponent()
            {
                
            }
        }
    }
}
