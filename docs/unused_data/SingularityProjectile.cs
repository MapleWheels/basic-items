﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Weapons.Projectiles
{
    public class SingularityProjectile : ModProjectile
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault(nameof(SingularityProjectile));
        }

        public override void SetDefaults()
        {
            projectile.width = 14;
            projectile.height = 14;
            projectile.alpha = 0;
            projectile.friendly = true;
            projectile.tileCollide = Config.Flags.Items.Weapons.SingularityProjectile.TileCollision;
            projectile.ignoreWater = Config.Flags.Items.Weapons.SingularityProjectile.IgnoreWater;
            projectile.ranged = true;
            ProjectileID.Sets.Homing[projectile.type] = true;
        }
    }
}
