﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using BasicItems.Core;

namespace BasicItems.Items.Weapons.Projectiles
{
    public class SingularityBullet : ModItem
    {
        public override void SetStaticDefaults()
        {
            base.SetStaticDefaults();
            DisplayName.SetDefault("[Confidential] Singularity");
            Tooltip.SetDefault("Zo[Confidential].");
        }

        public override void SetDefaults()
        {
            item.damage = Config.Flags.Items.Weapons.SingularityBullet.Damage;
            item.crit = Config.Flags.Items.Weapons.SingularityBullet.CriticalChance;
            item.width = 14;
            item.height = 14;
            item.maxStack = Config.Flags.Items.Weapons.SingularityBullet.MaxStackSize;
            item.consumable = true;
            item.knockBack = Config.Flags.Items.Weapons.SingularityBullet.Knockback;
            item.value = 6;
            item.rare = 8;
            item.shoot = mod.ProjectileType(nameof(SingularityProjectile));
            item.ammo = mod.ItemType(nameof(BasicWeapons.BasicBullet));
        }

        public override void AddRecipes()
        {
            if (Config._bProductionMode)
            {
                SingularityBulletRecipe bbr = new SingularityBulletRecipe(mod);
                bbr.AddIngredient(ItemID.HallowedBar, 1);
                bbr.AddIngredient(ItemID.SoulofMight, 2);
                bbr.AddIngredient(ItemID.SoulofFright, 2);
                bbr.AddIngredient(ItemID.SoulofSight, 2);
                bbr.AddTile(TileID.Anvils);
                bbr.SetResult(this, 50);
                bbr.AddRecipe();
            }
            else
            {
                ModRecipe dev = new ModRecipe(mod);
                dev.SetResult(this);
                dev.AddRecipe();
            }
        }
    }

    public class SingularityBulletRecipe : ModRecipe
    {
        public SingularityBulletRecipe(Mod mod) : base (mod)
        {

        }

        public override bool RecipeAvailable()
        {
            return (
                Main.LocalPlayer.HasItem(mod.ItemType("BasicGun")) ||
                Main.LocalPlayer.HasItem(mod.ItemType("IlluminatiPumperMachineGun"))
                );
        }

        public override int ConsumeItem(int type, int numRequired) 
        {
            return base.ConsumeItem(type, numRequired);
        }
    }
}
